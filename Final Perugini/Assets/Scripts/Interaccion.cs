using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interaccion : MonoBehaviour
{
    public string mensaje;

    public void InteractuarBase()
    {
        Interactuar();
    }
    
    protected virtual void Interactuar()
    {

    }
}
