using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    private InputJugador inputJugador;
    public InputJugador.EnPisoActions enPiso;

    private MotorJugador motor;
    private MirarJugador mirar;
    // Start is called before the first frame update
    void Awake()
    {
        inputJugador = new InputJugador();
        enPiso = inputJugador.EnPiso;
        motor = GetComponent<MotorJugador>();
        mirar = GetComponent<MirarJugador>();
        enPiso.Salto.performed += ctx => motor.Saltar();
        enPiso.Salir.performed += ctx => mirar.Salir();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        motor.ProcesarMov(enPiso.Movimiento.ReadValue<Vector2>());
        
    }

    private void LateUpdate()
    {
        mirar.ProcesarMirar(enPiso.Mirar.ReadValue<Vector2>());
    }

    private void OnEnable()
    {
        enPiso.Enable();
    }

    private void OnDisable()
    {
        enPiso.Disable();
    }
}
