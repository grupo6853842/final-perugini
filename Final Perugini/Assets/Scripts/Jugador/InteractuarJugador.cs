using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractuarJugador : MonoBehaviour
{
    private Camera cam;
    [SerializeField]
    private float distancia = 3f;
    [SerializeField]
    private LayerMask mascara;
    private UIJugador UIjug;
    private InputManager inputM;
    // Start is called before the first frame update
    void Start()
    {
        cam = GetComponent<MirarJugador>().cam;
        UIjug = GetComponent<UIJugador>();
        inputM = GetComponent<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        UIjug.ActualizarTexto(string.Empty);
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * distancia);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, distancia, mascara))
        {
            if(hitInfo.collider.GetComponent<Interaccion>() != null)
            {
                Interaccion interaccion = hitInfo.collider.GetComponent<Interaccion>();
                UIjug.ActualizarTexto(interaccion.mensaje);
                if (inputM.enPiso.Interactuar.triggered)
                {
                    interaccion.InteractuarBase();
                }
            }
        }
    }
}
