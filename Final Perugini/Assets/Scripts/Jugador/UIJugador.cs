using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIJugador : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textoMensaje;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ActualizarTexto(string texto)
    {
        textoMensaje.text = texto;
    }
}
