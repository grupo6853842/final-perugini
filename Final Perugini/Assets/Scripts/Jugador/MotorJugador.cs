using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorJugador : MonoBehaviour
{
    private CharacterController controlador;
    private Vector3 velocidadJugador;
    private bool enPiso;
    public float velocidad = 5f;
    public float gravedad = -9.8f;
    public float salto = 3f;

    // Start is called before the first frame update
    void Start()
    {
        controlador = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        enPiso = controlador.isGrounded;
    }

    public void ProcesarMov(Vector2 input)
    {
        //float movimientoAdelanteAtras = input.y * velocidad;
        //float movimientoCostados = Input.GetAxis("Horizontal") * velocidad;

        //movimientoAdelanteAtras *= Time.deltaTime;
        //movimientoCostados *= Time.deltaTime;

        //transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        Vector3 direccionMov = Vector3.zero;
        direccionMov.x = input.x;
        direccionMov.z = input.y;

        controlador.Move(transform.TransformDirection(direccionMov) * velocidad * Time.deltaTime);
        
        //gravedad
        velocidadJugador.y += gravedad * Time.deltaTime;
        if(enPiso && velocidadJugador.y < 0)
        {
            velocidadJugador.y = -2f;
        }
        controlador.Move(velocidadJugador * Time.deltaTime);
    }

    public void Saltar()
    {
        if (enPiso)
        {
            velocidadJugador.y = Mathf.Sqrt(salto * -3f * gravedad);
        }
    }
}
