using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonLab1 : Interaccion
{
    [SerializeField]
    private GameObject puerta;
    private bool puertaAbierta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Interactuar()
    {
        puertaAbierta = !puertaAbierta;
        puerta.GetComponent<Animator>().SetBool("IsOpen", puertaAbierta);
    }
}
